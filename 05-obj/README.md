# 目的檔、連結與載入

* [連結與載入](./doc/linkLoader.md)
* [使用 gcc 產生並觀察目的檔](./01-gcc)
* [使用 objdump 觀察目的檔](./02-objdump)
* [使用 gcc 進行 link 動作](./03-link)
* [GNU binutil 二進位工具](./04-binutil)
* [DLL 動態連結](./05-dll)
* [Linux 早期的 a.out 目的檔格式](./doc/objAout.md)
* [Linux 的 ELF 目的檔格式](./doc/elf.md)
